
using UnityEngine;

public class DisconnectFromGame : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_DisconnectPlayer;
    public void DisconnectPlayer()
    {
        m_DisconnectPlayer.Raise();
    }
}
