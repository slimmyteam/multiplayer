using System.Collections;
using Unity.Netcode;
using UnityEngine;

public class LobbyController : NetworkBehaviour
{
    [SerializeField]
    private NetworkVariable<int> m_ContadorPlayersReady = new(0, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Server);
    [SerializeField]
    private int m_SegundosEsperaCuentaAtras;

    private void Awake()
    {
        m_ContadorPlayersReady.Value = 0;
    }

    public void AumentarContadorReady()
    {
        m_ContadorPlayersReady.Value++;
        if (m_ContadorPlayersReady.Value == 2)
            StartCoroutine(StartMatchWhenMinimumReady());
    }

    public void DisminuirContadorReady()
    {
        m_ContadorPlayersReady.Value--;
        if (m_ContadorPlayersReady.Value < 2)
            StopAllCoroutines();
    }

    private IEnumerator StartMatchWhenMinimumReady()
    {
        int cont = m_SegundosEsperaCuentaAtras;
        for (int i = 0; i < m_SegundosEsperaCuentaAtras; i++)
        {
            Debug.Log(cont);
            cont--;
            yield return new WaitForSeconds(1);
        }
        GameManager.Instance.ChangeScene("Game", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
