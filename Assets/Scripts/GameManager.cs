using Unity.Netcode;
using UnityEngine.SceneManagement;

public class GameManager : NetworkBehaviour
{
    public static GameManager Instance;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(gameObject);
    }

    public void ChangeScene(string nameScene, LoadSceneMode mode)
    {
        NetworkManager.Singleton.SceneManager.LoadScene(nameScene, mode);
    }


}
