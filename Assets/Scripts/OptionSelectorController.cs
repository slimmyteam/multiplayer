using TMPro;
using UnityEngine;

public class OptionSelectorController : MonoBehaviour
{
    [SerializeField]
    private GEString m_EventOptionSelected;
    [SerializeField]
    private TextMeshProUGUI m_TextMeshProUGUI;

    public void OptionSelected()
    {
        m_EventOptionSelected.Raise(m_TextMeshProUGUI.text);
    }
}
