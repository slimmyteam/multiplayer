using UnityEngine;

public class DeactivateObject : MonoBehaviour
{
    public void DeactivateGameObject()
    {
        gameObject.SetActive(false);
    }
}
