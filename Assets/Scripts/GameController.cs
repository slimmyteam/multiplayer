using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : NetworkBehaviour
{
    private GameObject[] m_ArrayWayPoints = new GameObject[6];
    private readonly GameObject[] m_ArrayClientesConectados = new GameObject[6];

    [Header("Control de partida")]
    [SerializeField]
    private bool m_PartidaEnCurso = false;
    [SerializeField]
    private bool m_PlayerVivo = true;
    [SerializeField]
    private int m_TiempoEsperaEleccion = 10;
    [SerializeField]
    private List<Elecciones> m_Elecciones = new List<Elecciones>();
    [SerializeField]
    private int m_PlayersAliveCount = 1;

    [Header("Eventos")]
    [SerializeField]
    private GameEvent m_FinPartida;

    [Header("Referencias y prefabs")]
    [SerializeField]
    private GameObject m_LayoutEleccion;
    [SerializeField]
    private GameObject m_Canon;
    [SerializeField]
    private GameObject m_BalaPrefab;
    [SerializeField]
    private GameObject m_Camera;

    #region Iniciar partida
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        if (!IsServer)
            return;
        m_Camera = GameObject.Find("CameraContainer").transform.GetChild(0).gameObject;
        m_Camera.SetActive(false);
        m_PlayersAliveCount = NetworkManager.Singleton.ConnectedClientsList.Count;
        ColocarPersonajesYCambiarColorRpc();
    }

    /// <summary>
    /// Se buscan todos los objetos con tag "WayPoint" y se ordenan por nombre. Se llama a la funcion Init del player y se indica a cada jugador un waypoint donde debera colocarse.
    /// Llama a la corrutina que inicia la partida.
    /// </summary>
    private void ColocarPersonajesYCambiarColorRpc()
    {
        m_ArrayWayPoints = GameObject.FindGameObjectsWithTag("WayPoint");
        Array.Sort(m_ArrayWayPoints, CompareObjectsNames); //Ordenamos los waypoints por nombre        
        for (int i = 0; i < NetworkManager.Singleton.ConnectedClientsList.Count; i++)
        {
            m_ArrayClientesConectados[i] = NetworkManager.Singleton.ConnectedClientsList[i].PlayerObject.gameObject;
            m_ArrayClientesConectados[i].GetComponent<PlayerController>().InitPlayerRpc(m_ArrayWayPoints[i].transform.position);
        }
        m_PartidaEnCurso = true;
        StartCoroutine(Partida());
    }

    #endregion

    #region Gestion de Partida
    /// <summary>
    /// Bucle principal de la partida
    /// </summary>
    /// <returns></returns>
    private IEnumerator Partida()
    {
        while (m_PartidaEnCurso)
        {
            MostrarUIRpc();
            yield return new WaitForSeconds(m_TiempoEsperaEleccion); //Espera un tiempo para que players hagan elecciones
            OcultarUIRpc();
            yield return StartCoroutine(GirarCanon());

        }
    }

    /// <summary>
    /// Crea una nueva eleccion a partir de los parametros que envia el jugador y la a�ade a la lista de Elecciones
    /// </summary>
    /// <param name="eleccion">Nombre de la eleccion</param>
    /// <param name="uid">Id del jugador</param>
    public void AddToElectionList(string eleccion, ulong uid)
    {
        m_Elecciones.Add(new Elecciones(eleccion, uid));
    }

    /// <summary>
    /// Recorre la lista de Elecciones y comienza una corrutina por cada eleccion que gira el ca�on en la direccion indicada. Al final, limpia la lista de Elecciones y dispara.
    /// </summary>
    /// <returns></returns>
    private IEnumerator GirarCanon()
    {
        if (!IsServer)
            yield break;

        foreach (Elecciones eleccion in m_Elecciones)
        {
            yield return StartCoroutine(GirarCorutina(eleccion));
        }

        m_Elecciones.Clear();
        ShootCannonBall();
        yield return new WaitForSeconds(5);
    }

    /// <summary>
    /// Obtiene la rotaci�n inicial del ca�on y la rotacion final a partir de la direccion indicada por la eleccion. Llama a una nueva corrutina que lo hace girar.
    /// </summary>
    /// <param name="eleccion">Eleccion del jugador</param>
    /// <returns></returns>
    private IEnumerator GirarCorutina(Elecciones eleccion)
    {
        Vector3 rotacionFinal;
        Vector3 rotacionInicial = m_Canon.transform.rotation.eulerAngles;
        switch (eleccion.eleccion)
        {
            case "Izquierda":
                rotacionFinal = new Vector3(rotacionInicial.x, rotacionInicial.y - 60, rotacionInicial.z);
                break;
            case "Derecha":
                rotacionFinal = new Vector3(rotacionInicial.x, rotacionInicial.y + 60, rotacionInicial.z);
                break;
            case "Nada":
            default:
                rotacionFinal = rotacionInicial;
                break;
        }
        yield return StartCoroutine(LerpGiroCoroutine(rotacionInicial, rotacionFinal));
    }

    /// <summary>
    /// Gira el ca�on desde la rotacion inicial hasta la rotacion final mediante un Lerp.
    /// </summary>
    /// <param name="rotacionInicial">Rotacion inicial del canon</param>
    /// <param name="rotacionFinal">Rotacion final del canon</param>
    /// <returns></returns>
    private IEnumerator LerpGiroCoroutine(Vector3 rotacionInicial, Vector3 rotacionFinal)
    {
        float currentTime = 0;
        float lerpTime = 2f;
        while (currentTime <= lerpTime)
        {
            m_Canon.transform.eulerAngles = new Vector3(m_Canon.transform.eulerAngles.x, Mathf.Lerp(rotacionInicial.y, rotacionFinal.y, currentTime / lerpTime), m_Canon.transform.eulerAngles.z);
            yield return null;
            currentTime += Time.deltaTime;
        }
        yield return null;
    }

    /// <summary>
    /// Instancia una bala a partir del prefab y la dispara hacia el forward del ca�on.
    /// </summary>
    private void ShootCannonBall()
    {
        GameObject cannonBall = Instantiate(m_BalaPrefab);
        cannonBall.transform.position = m_Canon.transform.position;
        cannonBall.transform.forward = m_Canon.transform.forward;
        cannonBall.GetComponent<NetworkObject>().Spawn();
        cannonBall.GetComponent<Rigidbody>().velocity = cannonBall.transform.forward * 5;
    }

    //Desconexiones y muertes

    /// <summary>
    /// Indica que el player ha muerto, desactiva la UI y llama a la funcion para descontar un jugador de la partida
    /// </summary>
    public void PlayerDies()
    {
        m_PlayerVivo = false;
        m_LayoutEleccion.SetActive(false);
        DiscountPlayerAliveRpc();
    }

    /// <summary>
    /// Resta un jugador al contador de players vivos. Si solo queda uno, termina la partida y devuelve a Lobby.
    /// </summary>
    [Rpc(SendTo.Server)]
    private void DiscountPlayerAliveRpc()
    {
        m_PlayersAliveCount--;
        if (m_PlayersAliveCount == 1)
        {
            m_LayoutEleccion.SetActive(false);
            m_PartidaEnCurso = false;
            m_FinPartida.Raise();
            GameManager.Instance.ChangeScene("Lobby", UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }

    /// <summary>
    /// Llama a la funcion para descontar un jugador de la partida y devuelve al jugador a Inicio
    /// </summary>
    public void PlayerDisconnected()
    {
        DiscountPlayerAliveRpc();
        SceneManager.LoadScene("Inicio");
    }

    #endregion

    #region Gestion de UI

    /// <summary>
    /// Activa el layout de eleccion
    /// </summary>
    [Rpc(SendTo.ClientsAndHost)]
    private void MostrarUIRpc()
    {
        if (m_PlayerVivo)
            m_LayoutEleccion.SetActive(true);
    }

    /// <summary>
    /// Desactiva el layout de eleccion
    /// </summary>
    [Rpc(SendTo.ClientsAndHost)]
    private void OcultarUIRpc()
    {
        m_LayoutEleccion.SetActive(false);
    }

    #endregion


    #region Utiles

    private int CompareObjectsNames(GameObject a, GameObject b)
    {
        return a.name.CompareTo(b.name);
    }

    #endregion

}

public struct Elecciones
{
    public ulong id;
    public string eleccion;

    public Elecciones(string eleccion, ulong uid)
    {
        this.eleccion = eleccion;
        id = uid;
    }
}
