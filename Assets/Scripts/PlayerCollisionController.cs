using Unity.Netcode;
using UnityEngine;

public class PlayerCollisionController : NetworkBehaviour
{
    [SerializeField]
    private GameEvent m_PlayerDiesEvent;

    private void OnCollisionEnter(Collision collision)
    {
        if (!IsOwner)
            return;
        if (!collision.gameObject.CompareTag("Bala"))
        {
            if (collision.gameObject.CompareTag("Agua"))
            {
                GameObject camContainer = GameObject.Find("CameraContainer");
                camContainer.transform.GetChild(0).gameObject.SetActive(true);
                m_PlayerDiesEvent.Raise();
                DesactivatePlayerRpc();
            }
            else return;
        }
        else
        {
            collision.gameObject.GetComponent<BalaBehaviourController>().Despawnear();
            KnockBackRpc();
        }

    }

    [Rpc(SendTo.Server)]
    private void KnockBackRpc()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(-transform.forward * 1, ForceMode.Impulse);
    }

    [Rpc(SendTo.Server)]
    private void DesactivatePlayerRpc()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
