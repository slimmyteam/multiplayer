using Unity.Netcode;
using UnityEngine;

public class BalaBehaviourController : NetworkBehaviour
{
    private int m_WallsLayer;

    private void Awake()
    {
        m_WallsLayer = LayerMask.NameToLayer("Walls");
    }

    public void Despawnear()
    {
        DespawnearRpc();
    }

    [Rpc(SendTo.Server)]
    private void DespawnearRpc()
    {
        gameObject.GetComponent<NetworkObject>().Despawn();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == m_WallsLayer)
        {
            DespawnearRpc();
        }
    }
}
