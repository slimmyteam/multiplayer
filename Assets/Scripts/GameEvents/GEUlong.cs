using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Ulong")]
public class GEUlong : GEGenerico<ulong> { }
