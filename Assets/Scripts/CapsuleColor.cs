using UnityEngine;

public class CapsuleColor : MonoBehaviour
{
    [SerializeField]
    private GEColor m_EnvioColor;
    [SerializeField]
    private Color m_ColorAsignado;

    public void ChangeColor(Color color)
    {
        m_ColorAsignado = color;
        GetComponent<MeshRenderer>().material.color = m_ColorAsignado;
    }

    public void EnvioColor()
    {
        m_EnvioColor.Raise(m_ColorAsignado);
    }
}
