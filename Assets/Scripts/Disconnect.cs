
using UnityEngine;
using UnityEngine.SceneManagement;

public class Disconnect : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_DisconnectPlayer;
    public void DisconnectPlayer()
    {
        SceneManager.LoadScene("Inicio");
        m_DisconnectPlayer.Raise();
    }
}
