using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_InputFieldNickname;
    [SerializeField]
    private PlayerNicknameSO m_PlayerNicknameSO;

    public void StartHost()
    {
        if (CheckIfNicknameIsEmpty())
            return;

        NetworkManager.Singleton.StartHost();
        GuardarNickname(m_InputFieldNickname.text);
    }

    public void StartClient()
    {
        if (CheckIfNicknameIsEmpty())
            return;

        NetworkManager.Singleton.StartClient();
        GuardarNickname(m_InputFieldNickname.text);
    }

    private bool CheckIfNicknameIsEmpty()
    {
        if (m_InputFieldNickname.text.Length <= 1)
        {
            return true;
        }
        return false;
    }

    public void GuardarNickname(string nickname)
    {
        m_PlayerNicknameSO.nickname = nickname;
        SceneManager.LoadScene("Lobby");
    }
}
