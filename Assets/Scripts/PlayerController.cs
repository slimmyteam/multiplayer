using TMPro;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : NetworkBehaviour
{
    [Header("Personalizacion")]
    [SerializeField]
    private PlayerNicknameSO m_PlayerNicknameSO;
    [SerializeField] //SOLO el Owner puede escribir en la variable
    private NetworkVariable<FixedString128Bytes> m_Nickname = new NetworkVariable<FixedString128Bytes>(readPerm: NetworkVariableReadPermission.Everyone, writePerm: NetworkVariableWritePermission.Owner);
    [SerializeField]
    NetworkVariable<Color> m_ColorAsignado = new();

    [Header("Partida")]
    [SerializeField]
    private string m_DireccionSeleccionada;

    [Header("Referencias")]
    [SerializeField]
    private GameObject m_Camera;

    [Header("GameEvents")]
    [SerializeField]
    private GameEvent m_EventToNotifyReadyState;
    [SerializeField]
    private GameEvent m_EventToNotifyDisconnectState;
    [SerializeField]
    private GameEvent m_EventToHideUI;
    [SerializeField]
    private GEStringUlong m_SendChoiceToGameController;

    #region Lobby
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        NetworkManager.Singleton.OnClientDisconnectCallback += VolverAInicio; //Suscripcion para controlar los casos en que el host se desconecta o ocurre un error de conexion
    }

    /// <summary>
    /// Llama a una funcion rpc para comunicar al servidor. Asigna el nickname al jugador desde el ScriptableObject.
    /// </summary>
    /// <param name="color">Color escogido en el color picker del Lobby</param>
    public void PlayerReady(Color color)
    {
        if (!IsOwner)
            return;
        AvisarReadyRpc(color);
        m_Nickname.Value = m_PlayerNicknameSO.nickname;
    }

    /// <summary>
    /// Se guarda el color escogido en una NetworkVariable y lanza un evento para notificar que el jugador esta listo.
    /// </summary>
    /// <param name="color">Color escogido en el color picker del Lobby</param>
    [Rpc(SendTo.Server)]
    private void AvisarReadyRpc(Color color)
    {
        m_ColorAsignado.Value = color;
        m_EventToNotifyReadyState.Raise();
    }

    #endregion

    #region Partida

    /// <summary>
    /// Se activa el hijo de PlayerPrefab, que contiene el modelo del jugador, la camara y el nickname. Coloca al jugador en la posicion indicada, con el forward en direccion al ca�on.
    /// Se le asigna al material el color asignado, se activa la gravedad del Rigidbody y se muestra el nickname elegido en el texto. Se desactivan todas las camaras excepto la del Owner.
    /// </summary>
    /// <param name="position">Posicion designada por el Game Controller, que marca donde debe colocarse el player</param>
    [Rpc(SendTo.ClientsAndHost)]
    public void InitPlayerRpc(Vector3 position)
    {
        gameObject.transform.position = position;
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.transform.forward = Vector3.zero - transform.position;
        gameObject.GetComponentInChildren<MeshRenderer>().material.color = m_ColorAsignado.Value;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
        gameObject.transform.GetComponentInChildren<TextMeshProUGUI>().SetText(m_Nickname.Value.ToString());
        if (IsOwner)
            return;
        m_Camera.SetActive(false);
    }

    /// <summary>
    /// Llama a una funcion para enviar una peticion Rcp con la eleccion y la OwnerClientId. Lanza un evento para ocultar la UI.
    /// </summary>
    /// <param name="eleccion">Eleccion del jugador("Izquierda"/"Derecha"/"Nada"), recibida mediante un Game Event</param>
    public void EnviamosEleccionAServer(string eleccion)
    {
        if (!IsOwner)
            return;
        EnviarPeticionRpc(eleccion, OwnerClientId);
        m_EventToHideUI.Raise();
    }

    /// <summary>
    /// Envia una peticion Rcp al servidor donde comunica la eleccion y la id del jugador
    /// </summary>
    /// <param name="eleccion">Eleccion del jugador ("Izquierda"/"Derecha"/"Nada")</param>
    /// <param name="uid">OwnerClientId del jugador</param>
    [Rpc(SendTo.Server)]
    private void EnviarPeticionRpc(string eleccion, ulong uid)
    {
        //Meter en la lista de GameController el id del player y su eleccion
        m_SendChoiceToGameController.Raise(eleccion, uid);
    }

    /// <summary>
    /// Peticion Rpc a los clientes y host que desactiva la gravedad del PlayerParent y desactiva el hijo (que tambien contiene la camara y el nickname flotante)
    /// </summary>
    [Rpc(SendTo.ClientsAndHost)]
    public void EndGameRpc()
    {
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    #endregion

    #region Desconexiones

    /// <summary>
    /// Llama a la una funcion Rpc para notificar al servidor de la desconexion y realiza un shutdown.
    /// </summary>
    public void DisconnectPlayer()
    {
        if (!IsOwner)
            return;

        DisconnectPlayerRpc();
        NetworkManager.Singleton.Shutdown();
    }

    /// <summary>
    /// Lanza un evento en el servidor para notificar de la desconexion de un jugador
    /// </summary>
    [Rpc(SendTo.Server)]
    private void DisconnectPlayerRpc()
    {
        m_EventToNotifyDisconnectState.Raise();
    }

    /// <summary>
    /// Comprueba si el player desconectado es el servidor o el owner. Si lo es, realiza un shutdown y envia al jugador a la escena Inicio. 
    /// Esta funcion esta suscrita al NetworkManager.OnClientDisconnectCallback, se activa cuando ocurre un error de conexion.
    /// </summary>
    /// <param name="action"></param>
    private void VolverAInicio(ulong clientId)
    {
        if (!IsOwner || clientId != NetworkManager.ServerClientId || clientId != OwnerClientId)
            return;
        NetworkManager.Singleton.Shutdown();
        SceneManager.LoadScene("Inicio");
    }

    #endregion

}
