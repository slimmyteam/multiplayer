using UnityEngine;

[CreateAssetMenu(fileName = "PlayerNickname", menuName = "ScriptableObjects/PlayerNickname")]
public class PlayerNicknameSO : ScriptableObject
{
    public string nickname;
}
